package com.bestoneenc.kpopcon.controller.admin;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;



import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextHierarchy({
		@ContextConfiguration("classpath:com/bestoneenc/kpopcon/service/test-service-context.xml"),
	@ContextConfiguration("classpath:com/bestoneenc/kpopcon/test-servlet-context.xml") 
})
public class MemberAdminControllerTest {

	@Autowired
	private WebApplicationContext wac;
	private MockMvc mockMvc;
	
	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}

	@Test
	public void testList() throws Exception{
		String memberType ="G";
		mockMvc.perform(get("/admin/member/{type}/list",memberType)
				)
				.andExpect(status().isOk())
				;
	}
	
	@Test 
	public void testUpdateStatus() throws Exception{
		int id =1;
		mockMvc.perform(put("/admin/member/{id}/status",id)
				)
				.andExpect(status().isOk())
				;
	}

}
