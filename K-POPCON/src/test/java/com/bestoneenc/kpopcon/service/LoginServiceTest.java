package com.bestoneenc.kpopcon.service;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bestoneenc.kpopcon.controller.HomeController;
import com.bestoneenc.kpopcon.vo.Member;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("test-service-context.xml")
public class LoginServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(LoginService.class);

	@Autowired
	private LoginService loginService;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test 
	public void testIsEmail() throws Exception {
		String email = "nunssuby@sindoh.com";
		boolean result = loginService.isMember(email);
		assertTrue(result ==false);
		
		email = "nunssuby@kpopcon.com";
		result = loginService.isMember(email);
		assertTrue(result ==true);
	}

	@Test
	public void testIsLogin() throws Exception {
		String email = "nunssuby@kpopcon.com";
		String password = "1234";
		
		boolean result = loginService.isLogin(email, password);
		
//		logger.info("result is {}.", loginCheck);	
		
		assertTrue(result == true);
	}
	
	@Test
	public void testGetMember() throws Exception {
		String email = "nunssuby@kpopcon.com";
		Member member = loginService.getMember(email);
		
		assertThat(member.getPassword(), is("1234"));
		assertThat(member.getName(), is("김한성"));
		assertThat(member.getGender(), is("남성"));
		assertThat(member.getBirthday(), is("811206"));
		assertThat(member.getNationalityId(), is(1));
		assertThat(member.getImage(), is(nullValue()));
		assertThat(member.getLoginType(), is("KP"));
		assertThat(member.getBenefit(), is("1"));
		assertThat(member.getStatus(), is("OK"));
		assertThat(member.getThemeId(), is(2));
		
	}
	

}
