package com.bestoneenc.kpopcon.mapper;



import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bestoneenc.kpopcon.vo.Banner;
import com.bestoneenc.kpopcon.vo.Member;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("../service/test-service-context.xml")
public class MemberMapperTest {
	
	@Autowired
	MemberMapper memberMapper;
	String email;
	@Before
	public void setUp() throws Exception {
		email ="nunssuby@kpopcon.com";
	}

	@Test
	public void testGetMemberCount() {
		
		assertThat(memberMapper.selectMemberCount(email), is(1));
	}

	@Test
	public void testGetMember() {
		Member member = memberMapper.selectMember(email);
		assertThat(member.getBirthday(), is("811206"));
	}

	@Test
	public void testGetMemberById() {
		Member member = memberMapper.selectMemberById(1);
		assertThat(member.getBirthday(), is("811206"));
	}

	@Test
	public void testInsertMember() {
		Member member = new Member();
		member.setEmail("test@kpopcon.com");
		member.setBirthday("820222");
		member.setPassword("1234");
		member.setName("kpop");
		member.setGender("중성");
		member.setNationalityId(1);
		member.setLoginType("KR");
		member.setBenefit("1");
		member.setStatus("OK");
		member.setImage("image");
		memberMapper.insertMember(member);
		assertThat(member.getMemberId(),is(3));
	}
	
	/*@Test
	public void testInsertMemberForEmailAndPassword(){
		Member member = new Member();
		member.setEmail("eun@kpopcon.com");
		member.setPassword("1");
		memberMapper.insertMember(member);
		assertThat(member.getMemberId(),is(4));
	}*/
	
	@Test
	public void testDeleteMember(){
//		assertThat(memberMapper.deleteMember(1), is(1));
	}
	
	@Test
	public void testSelectMembers(){
		List<Member> members = memberMapper.selectMembers("G");
		assertThat(members.size(), is(3));
	}
	
	@Test
	public void testUpdateMember(){
		Member member = memberMapper.selectMember(email);
		member.setBirthday("820222");
		
		assertThat(memberMapper.updateMember(member),is(1));
		Member testMember = memberMapper.selectMember(email);
		assertThat(testMember.getBirthday(),is("820222"));
		
		member.setStatus("N");
		assertThat(memberMapper.updateMember(member),is(1));
		testMember = memberMapper.selectMember(email);
		assertThat(testMember.getStatus(), is("N"));
		
	}

}
