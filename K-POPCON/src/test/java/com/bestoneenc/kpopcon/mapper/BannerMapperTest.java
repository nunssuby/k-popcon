package com.bestoneenc.kpopcon.mapper;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bestoneenc.kpopcon.vo.Banner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("../service/test-service-context.xml")
public class BannerMapperTest {

	@Autowired
	BannerMapper bannerMapper;
	
	Banner banner;
	
	@Before
	public void setUp() throws Exception {
		banner = new Banner();
		
		banner.setSequence(1);
		banner.setTitle("제목");
		banner.setStartDate("2015-04-16");
		banner.setEndDate("2015-04-25");
		banner.setLanguage("한쿡어");
		banner.setIsValid("Y");
//		banner.setFile("file");
		banner.setImage("image");
		banner.setBannerType("main");
	}

	@Test
	public void testInsertBanner() {
		bannerMapper.insertBanner(banner);
		assertThat(banner.getBannerId(), is(2));
	}
	@Test
	public void testSelectBanner() {
		Banner banner = bannerMapper.selectBanner(1);
		assertThat(banner.getTitle(),is("메인배너"));
		assertThat(banner.getStartDate(),is("2015-04-16"));
	}
	@Test
	public void testUpdateBanner(){
		Banner banner = bannerMapper.selectBanner(1);
		banner.setTitle("제목 변경");
		assertThat(bannerMapper.updateBanner(banner), is(1));
		Banner updatedBanner = bannerMapper.selectBanner(1);
		assertThat(updatedBanner.getTitle(), is("제목 변경"));
	}
	@Test
	public void testDeleteBanner(){
		assertThat(bannerMapper.deleteBanner(1), is(1));
	}
	
	@Test
	public void testSelectBannsers(){
		List<Banner> banners = bannerMapper.selectBanners("main");
		Banner firstBanner = banners.get(0);
		assertThat(firstBanner.getTitle(), is("메인배너"));
	}

}
