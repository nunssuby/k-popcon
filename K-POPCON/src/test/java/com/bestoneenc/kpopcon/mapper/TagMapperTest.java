package com.bestoneenc.kpopcon.mapper;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bestoneenc.kpopcon.vo.Tag;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("../service/test-service-context.xml")
public class TagMapperTest {
	
	@Autowired
	TagMapper tagMapper;

	@Before
	public void setUp() throws Exception {
	}
	
	@Test
	public void testInsertTag(){
		
		Tag tag = new Tag();
		tag.setContentId(4);
		tag.setTag("인서트테스트");
		
		tagMapper.insertTag(tag);
		assertThat(tag.getTagId(), is(4));
	}

	@Test
	public void testSelectTagsByContent() {
		
		List<Tag> contentTags = tagMapper.selectTagsByContent(1);

		assertThat(contentTags.get(0).getTag(), is("DB태그1"));
		assertThat(contentTags.get(1).getTag(), is("DB태그2"));
		assertThat(contentTags.get(2).getTag(), is("DB태그3"));
	}
	@Test
	public void testDeleteTag(){
		Tag tag = new Tag();
		tag.setContentId(3);
		tag.setTag("인서트테스트");
		tagMapper.insertTag(tag);
		assertThat(tagMapper.deleteTag(tag.getTagId()), is(1));
	}
	@Test
	public void testDeleteTags(){
		assertThat(tagMapper.deleteTags(1), is(3));
		assertThat(tagMapper.selectTagsByContent(1).size(), is(0));
	}

}
