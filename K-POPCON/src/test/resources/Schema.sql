create table members(
    member_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    email varchar(50),
    password varchar(50),
    name varchar(50),
    gender varchar(50),
    birthday date,
    nationality_id INT(50),
    image varchar(50),
    login_type varchar(50),
    benefit varchar(50),
    status varchar(50),
    theme_id INT(50),
    member_type varchar(50),
    created_by BIGINT,
    creation_date datetime,
    last_updated_by BIGINT,
    last_update_date datetime,
    UNIQUE KEY(email)
    );
    
 create table contents(
    content_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    content_type varchar(10),
    member_id BIGINT,
    category_id INT,
    title varchar(50),
    youtube_link varchar(50),
    image_url varchar(50),
    description varchar(50),
    scope_id INT(50),
    likability varchar(50),
    share_count INT,
    benefit varchar(50),
    status varchar(50),
    is_best varchar(50),
    created_by BIGINT,
    creation_date datetime,
    last_updated_by BIGINT,
    last_update_date datetime,
    FOREIGN KEY (member_id) REFERENCES members(member_id) 
    );   
    
 create table content_tags(
    tag_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    content_id BIGINT,
    tag varchar(50),
    created_by BIGINT,
    creation_date datetime,
    last_updated_by BIGINT,
    last_update_date datetime,
    FOREIGN KEY (content_id) REFERENCES contents(content_id) 
    );
    
 create table content_scopes(
    content_scope_id BIGINT NOT NULL,
    name varchar(50),
    created_by BIGINT,
    creation_date datetime,
    last_updated_by BIGINT,
    last_update_date datetime,
    UNIQUE KEY(content_scope_id)
    ); 
    
 create table content_files(
 	file_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,	
 	content_id BIGINT,
 	original_name varchar(200),
 	uuid_name varchar(200),
 	extension varchar(200),
 	created_by BIGINT,
    creation_date datetime,
    last_updated_by BIGINT,
    last_update_date datetime,
 	FOREIGN KEY (content_id) REFERENCES contents(content_id) 
 	);
 
 create table categories(
 	category_id INT NOT NULL PRIMARY KEY,
 	name varchar(50),
    created_by BIGINT,
    creation_date datetime,
    last_updated_by BIGINT,
    last_update_date datetime,
 	UNIQUE KEY(category_id)
 	);

create table contents_box(
	box_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	member_id BIGINT,
	name varchar(50),
	created_by BIGINT,
    creation_date datetime,
    last_updated_by BIGINT,
    last_update_date datetime,
	FOREIGN KEY (member_id) REFERENCES members(member_id) 
	);

create table foder_contents_assignment(
	assignment_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	box_id BIGINT,
	content_id BIGINT,
	created_by BIGINT,
    creation_date datetime,
    last_updated_by BIGINT,
    last_update_date datetime,
	FOREIGN KEY (box_id) REFERENCES contents_box(box_id),
	FOREIGN KEY (content_id) REFERENCES contents(content_id)
	);
	
create table banners(
	banner_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	sequence INT,
	title varchar(50),
	start_date datetime,
	end_date datetime,
	language varchar(10),
	is_valid varchar(5),
	file varchar(50),
	link varchar(50),
	image varchar(50),
	banner_type varchar(50),
	creation_date datetime,
    last_updated_by BIGINT,
    last_update_date datetime
);
    
    