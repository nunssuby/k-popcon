package com.bestoneenc.kpopcon.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.bestoneenc.kpopcon.controller.ContentController;
import com.bestoneenc.kpopcon.vo.Member;

public class LoginInterceptor extends HandlerInterceptorAdapter {

	private static final Logger logger = LoggerFactory.getLogger(LoginInterceptor.class);
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

//		System.out.println("AnimalInterceptor: REQUEST Intercepted for URI: " + request.getRequestURI());

		HttpSession session = request.getSession();
		Member member = (Member) session.getAttribute("member");

		if (member == null) {
//			System.out.println("Interceptor : Session Check Fail");
//			response.sendRedirect("redirect:/login/loginForm");
			String contextPath = request.getContextPath();
//			logger.debug("contextPath : {}", contextPath);
			response.sendRedirect(contextPath+"/login/loginForm");
//			response.
			return false;
		} else {
//			System.out.println("Interceptor : Session Check true");
			return super.preHandle(request, response, handler);
		}

	}

	// /**
	// * Controller의 메소드가 수행이 완료되고, View 를 호출하기 전에 호출됩니다.
	// */
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
		// Nothing to do
		System.out.println("postHandle: REQUEST Intercepted for URI: " + request.getRequestURI());
	}
	//
	// /**
	// * View 작업까지 완료된 후 Client에 응답하기 바로 전에 호출됩니다.
	// */
	// @Override
	// public void afterCompletion(HttpServletRequest request,
	// HttpServletResponse response,
	// Object handler,
	// Exception ex) throws Exception {
	// }

}