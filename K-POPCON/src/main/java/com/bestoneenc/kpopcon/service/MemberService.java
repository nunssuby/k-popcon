package com.bestoneenc.kpopcon.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bestoneenc.kpopcon.mapper.MemberMapper;
import com.bestoneenc.kpopcon.vo.Member;

@Service
public class MemberService {
	@Autowired
	MemberMapper memberMapper;
	
	public List<Member> getMembers(String memberType){
		return memberMapper.selectMembers(memberType);
	}
	
	public Member getMember(String email){
		return memberMapper.selectMember(email);
	}
	
	public Member getMember(int memberId){
		return memberMapper.selectMemberById(memberId);
	}
	
	public int updateMember(Member member){
		return memberMapper.updateMember(member);
	}
	
	public Member createMember(Member member){
		memberMapper.insertMember(member);
		return member;
	}
}
