package com.bestoneenc.kpopcon.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bestoneenc.kpopcon.mapper.BannerMapper;
import com.bestoneenc.kpopcon.vo.Banner;

@Service
public class BannerService {

	@Autowired
	BannerMapper bannerMapper;
	
	public Banner createBanner(Banner banner){
		bannerMapper.insertBanner(banner);
		return banner;
	}
	public Banner getBanner(int bannerId){
		return bannerMapper.selectBanner(bannerId);
	}
	public int updateBanner(Banner banner){
		return bannerMapper.updateBanner(banner);
	}
	public int deleteBanner(Banner banner){
		return bannerMapper.deleteBanner(banner.getBannerId());
	}
	public int deleteBanner(int bannerId){
		return bannerMapper.deleteBanner(bannerId);
	}
	public List<Banner> getBanners(String bannerType){
		return bannerMapper.selectBanners(bannerType);
	}
}
