package com.bestoneenc.kpopcon.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bestoneenc.kpopcon.mapper.AttachedFileMapper;
import com.bestoneenc.kpopcon.vo.AttachedFile;

@Service
public class AttachedFileService {

	@Autowired
	AttachedFileMapper attachedFileMapper;
	
	public AttachedFile getAttachedFile(int fileId){
		return attachedFileMapper.selectFile(fileId);
	}
	
}
