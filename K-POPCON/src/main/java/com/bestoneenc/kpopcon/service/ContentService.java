package com.bestoneenc.kpopcon.service;

import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.bestoneenc.kpopcon.controller.ContentController;
import com.bestoneenc.kpopcon.mapper.ContentMapper;
import com.bestoneenc.kpopcon.mapper.AttachedFileMapper;
import com.bestoneenc.kpopcon.mapper.TagMapper;
import com.bestoneenc.kpopcon.vo.Category;
import com.bestoneenc.kpopcon.vo.Content;
import com.bestoneenc.kpopcon.vo.ContentBox;
import com.bestoneenc.kpopcon.vo.AttachedFile;
import com.bestoneenc.kpopcon.vo.Scope;
import com.bestoneenc.kpopcon.vo.Tag;
import com.bestoneenc.kpopcon.vo.Member;

@Service
public class ContentService {
	
	private static final Logger logger = LoggerFactory.getLogger(ContentService.class);

	@Autowired
	ContentMapper contentMapper;
	@Autowired
	TagMapper tagMapper;
	@Autowired
	AttachedFileMapper fileMapper;
	
	
	
	@Transactional(rollbackFor={Exception.class})
	public Content createContent(Content content) {

		contentMapper.insertContent(content);
		if (content.isContent()) {
			logger.debug("tag count : {}", content.getTags().size());
			for (Tag tag : content.getTags()) {
				tag.setContentId(content.getContentId());
				tagMapper.insertTag(tag);
			}
			logger.debug("file count : {}", content.getContentFiles().size());
			for (AttachedFile file : content.getContentFiles() ){
				file.setContentId(content.getContentId());
				fileMapper.insertFile(file);
			}
		}

		return content;
	}
	public Content getContent(int contentId){
		return contentMapper.selectContent(contentId);
	}
	public int deleteContent(Content content){
		
		tagMapper.deleteTags(content.getContentId());
		fileMapper.deleteFiles(content.getContentId());
		return contentMapper.deleteContent(content.getContentId());
	}
	public List<Content> getContents(){
		
		List<Content> contests = contentMapper.selectContents();
		for(Content content:contests){
			List<Tag> tags = tagMapper.selectTagsByContent(content.getContentId());
			content.setTags(tags);
			List<AttachedFile> contentFiles = fileMapper.selectFilesByContent(content.getContentId());
			content.setContentFiles(contentFiles);
		}
		
		return contests;
	}
	
	
	public List<Tag> getTagsByContent(int contentId){
		List<Tag> contentTags = tagMapper.selectTagsByContent(contentId);
		return contentTags;
	}
	
	public Scope getContentScope(int scopeId) {

		Scope contentScope = contentMapper.selectScope(scopeId);

		return contentScope;
	}
	public List<Scope> getContentScopes(){
		return contentMapper.selectScopes();
	}

	public ContentBox getContentBox(int contentBoxId) {

		ContentBox contentBox = new ContentBox();
		return contentBox;

	}
	
	public List<ContentBox> getContentBoxes(Member member){
		List<ContentBox> contentBoxes = contentMapper.selectContentBoxByMember(member.getMemberId());
		return contentBoxes;
	}
	
	public List<Category> getCategories(){
		return contentMapper.selectCategories();
	}
	
	

}
