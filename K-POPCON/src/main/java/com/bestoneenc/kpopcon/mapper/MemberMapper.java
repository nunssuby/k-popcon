package com.bestoneenc.kpopcon.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bestoneenc.kpopcon.vo.Member;

@Repository
public interface MemberMapper {
	public int selectMemberCount(String email);
	public Member selectMember(String email);
	public Member selectMemberById(int memberId);
	public void insertMember(Member member);
	public List<Member> selectMembers(String memberType);
	public int deleteMember(int memberId);
	public int updateMember(Member member);
}
