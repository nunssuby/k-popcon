package com.bestoneenc.kpopcon.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bestoneenc.kpopcon.vo.AttachedFile;

@Repository
public interface AttachedFileMapper {
	public int insertFile(AttachedFile file);
	public AttachedFile selectFile(int fileId);
	public List<AttachedFile> selectFilesByContent(int contentId);
	public int deleteFile(int fileId);
	public int deleteFiles(int contentId);
}
