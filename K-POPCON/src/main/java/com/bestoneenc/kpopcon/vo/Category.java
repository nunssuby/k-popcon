package com.bestoneenc.kpopcon.vo;

public enum Category {
	Sports(1), News(2), Hahaha(3);
	
	private final int value;
	
	Category(int value){
		this.value = value;
	}
	
	public int getValue(){
		return value;
	}
	
	public static Category valueOf(int value){
		switch(value){
		case 1 : return Sports;
		case 2 : return News;
		case 3 : return Hahaha;
		default : throw new AssertionError("Unknown value : " + value);
		}
	}
}
