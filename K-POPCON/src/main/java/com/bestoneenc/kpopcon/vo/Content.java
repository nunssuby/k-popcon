package com.bestoneenc.kpopcon.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

public class Content implements Serializable{
	
	int contentId;
	
	String title;
	String contentType;
	String ImageUrl;
	String youtubeLink;
	Category category;
	String description;
	ContentBox contentBox;
	String tagWithComma;
	List<Tag> tags;
	Scope scope;
	Member member;
	
	String likability;
	int shareCount;
	String benefit;
	String status;
	String isBest;
	String creationDate;
	
	List<AttachedFile> contentFiles;
	List<MultipartFile> files;

	public Content(){
		member = new Member();
		contentFiles = new ArrayList();
		files = new ArrayList();
		tags = new ArrayList();
	}
	
	
	public int getContentId() {
		return contentId;
	}
	public void setContentId(int contentId) {
		this.contentId = contentId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public String getImageUrl() {
		return ImageUrl;
	}
	public void setImageUrl(String imageUrl) {
		ImageUrl = imageUrl;
	}
	public String getYoutubeLink() {
		return youtubeLink;
	}
	public void setYoutubeLink(String youtubeLink) {
		this.youtubeLink = youtubeLink;
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	public int getCategoryId(){
		return category.getValue();
	}
	
	public void setCategoryId(int categoryId){
		setCategory(Category.valueOf(categoryId));
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public ContentBox getContentBox() {
		return contentBox;
	}
	public void setContentBox(ContentBox contentBox) {
		this.contentBox = contentBox;
	}
	public String getTagWithComma() {
		return tagWithComma;
	}
	public void setTagWithComma(String tagWithComma) {
		this.tagWithComma = tagWithComma;
	}
	public List<Tag> getTags() {
		return tags;
	}
	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}
	public Scope getScope() {
		return scope;
	}
	public void setScope(Scope scope) {
		this.scope = scope;
	}
	public int getScopeId(){
		return scope.getValue();
	}
	public void setScopeId(int scopeId){
		setScope(Scope.valueOf(scopeId));
	}
	
	public Member getMember() {
		return member;
	}
	public void setMember(Member member) {
		this.member = member;
	}
	public int getMemberId(){
		return member.getMemberId();
	}
	public void setMemberId(int memberId){
		member.setMemberId(memberId);
	}
	
	
	
	public String getLikability() {
		return likability;
	}
	public void setLikability(String likability) {
		this.likability = likability;
	}
	public int getShareCount() {
		return shareCount;
	}
	public void setShareCount(int shareCount) {
		this.shareCount = shareCount;
	}
	public String getBenefit() {
		return benefit;
	}
	public void setBenefit(String benefit) {
		this.benefit = benefit;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getIsBest() {
		return isBest;
	}
	public void setIsBest(String isBest) {
		this.isBest = isBest;
	}
	public String getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(String CreationDate) {
		this.creationDate = CreationDate;
	}
	public List<AttachedFile> getContentFiles() {
		return contentFiles;
	}
	public void setContentFiles(List<AttachedFile> contentFiles) {
		this.contentFiles = contentFiles;
	}
	public List<MultipartFile> getFiles() {
		return files;
	}
	public void setFiles(List<MultipartFile> files) {
		this.files = files;
	}
	public boolean isContent(){
		if(contentId>0)
			return true;
		return false;
	}
	
	public void tagWithCommaConvertToTagsInList(){
		System.out.println("tagWithComma :"+tagWithComma);
		if(tagWithComma != null){
			String[] tags = tagWithComma.split(",");
			System.out.println("size :"+tags.length);
			
			for (String tag:tags){
				System.out.println("tag :"+tag);
				Tag contentTag = new Tag();
				contentTag.setTag(tag);
				this.tags.add(contentTag);
			}
		}
	}
	
//	public List<Tag> stringArrayToListConvertForTag(String[] tagArray){
//		List<Tag> tags = new ArrayList<Tag>();
//		
//		for (String tag:tagArray){
//			Tag contentTag = new Tag();
//			contentTag.setTag(tag);
//			tags.add(contentTag);
//		}
//		
//		return tags;
//	}
	
}
