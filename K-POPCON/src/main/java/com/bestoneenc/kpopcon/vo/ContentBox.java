package com.bestoneenc.kpopcon.vo;

import java.util.List;

public class ContentBox {
	int boxId;
	List<Content> contents;
	Member member;
	String name;
	
	public ContentBox(){
		this.boxId = 0;
		member = new Member();
	}
	
	public int getBoxId() {
		return boxId;
	}
	public void setBoxId(int boxId) {
		this.boxId = boxId;
	}
	public List<Content> getContents() {
		return contents;
	}
	public void setContents(List<Content> contents) {
		this.contents = contents;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}
	
	public int getMemberId(){
		return member.getMemberId();
	}
	public void setMemberId(int memberId){
		this.member.setMemberId(memberId);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
