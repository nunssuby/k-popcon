package com.bestoneenc.kpopcon.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bestoneenc.kpopcon.mapper.MemberMapper;
import com.bestoneenc.kpopcon.service.ContentService;
import com.bestoneenc.kpopcon.service.LoginService;
import com.bestoneenc.kpopcon.vo.Content;
import com.bestoneenc.kpopcon.vo.Member;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {

	@Autowired
	LoginService loginService;
	@Autowired
	ContentService contentService;

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView home(Locale locale, Model model) {

		ModelAndView mav = new ModelAndView("/home/main");
		List<Content> contents = contentService.getContents();
		mav.addObject("contents",contents);
		
		return mav;
	}

	@RequestMapping(value = "/login/loginForm", method = RequestMethod.GET)
	public String showLoginForm() {

		System.out.println("LoginForm");
		return "login/loginForm";
	}

	@RequestMapping(value = "/login/loginResult", method = RequestMethod.POST)
	public @ResponseBody Map isLogin(HttpServletRequest request, 
									HttpServletResponse response, 
									@RequestParam(value = "email") String email, 
									@RequestParam(value = "password") String password)
											throws IOException {
		
		Map<String, Boolean> result = new HashMap<String, Boolean>();

		try {
			boolean isLogin = loginService.isLogin(email, password);
			Member member = loginService.getMember(email);
			if (isLogin) {
				createSessionForMember(request,member);
			}

			result.put("result", isLogin);
		} catch (DataAccessException e) {
			result.put("result", false);
			result.put("dataException", true);
		}

		return result;

	}
	
	private void createSessionForMember(HttpServletRequest request, Member member){
		HttpSession session = request.getSession();
		if(session.isNew() || session.getAttribute("member")== null){
			session.setAttribute("member", member);
		} else {
			// TODO 이렇게 안되게 구현하기
			System.out.println("isLogin ok session 생성안된기라");
		}
	}
	

	@RequestMapping(value = "/facebooktest", method = RequestMethod.GET)
	public String facebooktest(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);

		return "facebooktest";
	}

	@RequestMapping(value = "/googleplustest", method = RequestMethod.GET)
	public String googleplustest(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);

		return "googleplustest";
	}

	@RequestMapping(value = "/twittertest", method = RequestMethod.GET)
	public String twittertest(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);

		return "twittertest";
	}

	@RequestMapping(value = "/instagramtest", method = RequestMethod.GET)
	public String instagramtest(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);

		return "instagramtest";
	}

	@RequestMapping(value = "/instagramredirecttest", method = RequestMethod.GET)
	public String instagramredirecttest(Locale locale, Model model) {
		logger.info("redirect");
//		logger.info("access_token : "+access_token);

		
		
		return "instagramredirecttest";
	}
	
	
	@RequestMapping(value = "/snsTest", method = RequestMethod.GET)
	public String snsTest(Locale locale, Model model) {
//		logger.info("access_token : "+access_token);

		
		
		return "/test/snsTest";
	}
	
	@RequestMapping(value = "test/main", method = RequestMethod.GET)
	public String mainTest(Locale locale, Model model) {
//		logger.info("access_token : "+access_token);

		
		
		return "/test/main";
	}

}
