package com.bestoneenc.kpopcon.controller.admin;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.util.StringUtils;

import com.bestoneenc.kpopcon.service.BannerService;
import com.bestoneenc.kpopcon.vo.Banner;
import com.bestoneenc.kpopcon.vo.Category;
import com.bestoneenc.kpopcon.vo.ContentBox;
import com.bestoneenc.kpopcon.vo.Member;
import com.bestoneenc.kpopcon.vo.Scope;


@Controller
@RequestMapping("/admin/banner/**")
public class BannerController {

	@Autowired
	BannerService bannerService;
	
	@RequestMapping(value = "/admin/banner/form", method = RequestMethod.GET)
	public ModelAndView form(
			) {
		ModelAndView mav = new ModelAndView("/admin/banner/create");
		return mav;
	}
	
	@RequestMapping(value = "/admin/banner/{id}/form", method = RequestMethod.GET)
	public ModelAndView updateForm(
			@PathVariable("id") int bannerId
			) {
		Banner banner = new Banner();
		ModelAndView mav = new ModelAndView("/admin/banner/create");
		banner = bannerService.getBanner(bannerId);
			 
		mav.addObject("banner", banner);
		
		return mav;
	}
	
	
	@RequestMapping(value="/admin/banner", method = RequestMethod.POST)
	public ModelAndView create(
			/* @RequestParam("title") String title
			, @RequestParam(value="sequence",defaultValue="0") Integer sequence
			, @RequestParam("startDate") String startDate
			, @RequestParam("endDate") String endDate
			, @RequestParam("language") String language
			, @RequestParam("isValid") String isValid
			, @RequestParam("link") String link
			,@RequestParam("bannerType") String bannerType
			,@RequestParam("file") MultipartFile file) throws IOException {*/
		
		
		@ModelAttribute("banner") Banner banner, BindingResult result){
		
		/*Banner banner = new Banner();
		banner.setTitle(title);
		banner.setSequence(sequence);
		banner.setStartDate(startDate);
		banner.setEndDate(endDate);
		banner.setLanguage(language);
		banner.setIsValid(isValid);
		banner.setLink(link);
		banner.setBannerType(bannerType);*/
		
		System.out.println("title : "+banner.getTitle());
		System.out.println("file : "+banner.getFile().getOriginalFilename());
		System.out.println("Extension : "+StringUtils.getFilenameExtension(banner.getFile().getOriginalFilename()));
		String genId = UUID.randomUUID().toString();
		System.out.println("genId : "+genId);
//		StringUtils.
		banner.setUuidFileName(genId+"."+StringUtils.getFilenameExtension(banner.getFile().getOriginalFilename()));
		bannerService.createBanner(banner);
		ModelAndView mav = new ModelAndView("redirect:/admin/banner/"+banner.getBannerId());
		
		mav.addObject("banner", banner);
		
		return mav;
	}
	
	/*@RequestMapping(value="/banner/Banner", method=RequestMethod.GET)
	public ModelAndView showBanner(
			 @RequestParam("bannerId") int bannerId
			){
		ModelAndView mav = new ModelAndView("/banner/show");
		Banner banner = bannerService.getBanner(bannerId);
		mav.addObject("banner",banner);
		
		return mav;
	}*/
	@RequestMapping(value="/admin/banner/{id}", method=RequestMethod.GET)
	public ModelAndView show(
			@PathVariable("id") int bannerId
			){
		ModelAndView mav = new ModelAndView("/admin/banner/show");
		Banner banner = bannerService.getBanner(bannerId);
		mav.addObject("banner",banner);
		
		return mav;
	}
	
	
	@RequestMapping(value="/admin/banner/{id}", method=RequestMethod.DELETE)
	public @ResponseBody Map<String, Integer> delete(
			@PathVariable("id") int bannerId
			){
		Map<String, Integer> result = new HashMap<String, Integer>();
		result.put("result", bannerService.deleteBanner(bannerId));
		return result;
	}
//	@RequestMapping(value="/banner/Banner/{bannerId}", method=RequestMethod.DELETE)
//	public @ResponseBody Map deleteBanner(
//			@PathVariable int bannerId
//			){
//		Map<String, Integer> result = new HashMap<String, Integer>();
//		result.put("result", bannerService.deleteBanner(bannerId));
//		return result;
//	}
	
/*	@RequestMapping(value="/admin/banner", method= RequestMethod.GET)
	public ModelAndView list(
//			@PathVariable("bannerType") String bannerType
			){
		
		ModelAndView mav = new ModelAndView("/admin/banner/list");
		String bannerType="main";
		List<Banner> banners = bannerService.getBanners(bannerType);
		
		mav.addObject("banners",banners);
		
		return mav;
	}*/
	
	@RequestMapping(value="/admin/banner/{type}/list", method=RequestMethod.GET)
	public ModelAndView list(
			@PathVariable("type") String bannerType
			){
		ModelAndView mav = new ModelAndView("/admin/banner/list");
		List<Banner> banners = bannerService.getBanners(bannerType);
		
		mav.addObject("banners",banners);
		
		return mav;
	}
	/*@RequestMapping(value="/banner", method= RequestMethod.GET)
	public ModelAndView showBanners(
			 @RequestParam("bannerType") String bannerType
			){
		
		ModelAndView mav = new ModelAndView("/banner/list");
		List<Banner> banners = bannerService.getBanners(bannerType);
		
		mav.addObject("banners",banners);
		
		return mav;
	}*/
	
}
