package com.bestoneenc.kpopcon.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/auth/**")
public class AuthController {

	
//	스토리보드 page2
	@RequestMapping(value = "/auth/join", method = RequestMethod.GET)
	public ModelAndView form(
			) {
		ModelAndView mav = new ModelAndView("/auth/join");
		return mav;
	}
	
	@RequestMapping(value = "/auth/facebook/callback", method = RequestMethod.GET)
	public ModelAndView facebookCallback(
			) {
		ModelAndView mav = new ModelAndView("/auth/facebook/callback");
		return mav;
	}
	
	@RequestMapping(value = "/auth/facebook", method = RequestMethod.GET)
	public ModelAndView facebook(
			) {
		ModelAndView mav = new ModelAndView("/auth/facebook");
		return mav;
	}
	
	@RequestMapping(value = "/auth/google", method = RequestMethod.GET)
	public ModelAndView google(
			) {
		ModelAndView mav = new ModelAndView("/auth/google");
		return mav;
	}
	
	@RequestMapping(value = "/auth/instagram", method = RequestMethod.GET)
	public ModelAndView instagram(
			) {
		ModelAndView mav = new ModelAndView("/auth/instagram");
		return mav;
	}
}
