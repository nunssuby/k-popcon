package com.bestoneenc.kpopcon.controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bestoneenc.kpopcon.service.MemberService;
import com.bestoneenc.kpopcon.vo.Banner;
import com.bestoneenc.kpopcon.vo.Member;

@Controller
@RequestMapping("/member/**")
public class MemberController {
	
	private static final Logger logger = LoggerFactory.getLogger(MemberController.class);
	
	@Autowired
	private MemberService memberService;

	@RequestMapping(value = "/member/form", method = RequestMethod.GET)
	public ModelAndView form(
			) {
		ModelAndView mav = new ModelAndView("/member/create");
		return mav;
	}
	
	@RequestMapping(value ="/member", method =RequestMethod.POST)
	public @ResponseBody Map<String,String> create(
			@ModelAttribute("member") Member member, BindingResult result){
		
		Map<String,String> resultMap = new HashMap();
		logger.debug("aaaaaa aaaaaaaaaaaaaaaaaaaaaaa" );
		Member existMember = memberService.getMember(member.getEmail());
		if(existMember ==null){
			logger.debug("cccccccccccccccccccccccccc " );
			memberService.createMember(member);
			logger.debug("memberId {}.", member.getMemberId());
			resultMap.put("memberId", Integer.toString(member.getMemberId()));
		}
		else{
			resultMap.put("isMember", "true");
		}
		
//		try{
//		}catch (Exception e){
//			logger.debug("error {}.", e.getStackTrace());
//		}
		return resultMap;
		
	}
	
	@RequestMapping(value="/member/{id}",method=RequestMethod.GET)
	public ModelAndView show(
			@PathVariable("id") int memberId
			){
		ModelAndView mav = new ModelAndView("/member/show");
		Member member = memberService.getMember(memberId);
		mav.addObject("member",member);
		
		return mav;
	}
	
	@RequestMapping(value = "/member/{id}/information/form", method = RequestMethod.GET)
	public ModelAndView updateInformationForm(
			@PathVariable("id") int memberId
			) {
		Member member = memberService.getMember(memberId);
		ModelAndView mav = new ModelAndView("/member/update");
			 
//		mav.addObject("member", member);
		
		return mav;
	}
//	@RequestMapping(value = "/member/information",method={RequestMethod.POST,RequestMethod.PUT}, params="_method=PUT")
	@RequestMapping(value = "/member/information", method = RequestMethod.PUT)
	public ModelAndView  updateInformation(
			@ModelAttribute("member") Member member, BindingResult result
			) {
		
		if ( result.hasErrors() ){
			 
			logger.debug( "getName {}.",result.getAllErrors() );
			 
			}
		
		logger.debug("getName {}.", member.getName());
		logger.debug("getGender {}.", member.getGender());
		logger.debug("birthday {}.", member.getBirthday());
		logger.debug("getNationalityId {}.", member.getNationalityId());
		
		//Member member = memberService.getMember(memberId);
		Member updateMember = memberService.getMember(member.getMemberId());
		
		updateMember.setBirthday(member.getBirthday());
		updateMember.setNationalityId(member.getNationalityId());
		updateMember.setGender(member.getGender());
		updateMember.setName(member.getName());
		
		if(memberService.updateMember(updateMember)>0){
			if("KP".equals(member.getMemberType())){
//				TODO : 인증메일 발송
			} 
			
		}
		
		ModelAndView mav = new ModelAndView("login/loginForm");
		mav.addObject("member", member);
		
		return mav;
	}
	
	@RequestMapping(value="/member/email/{email}",method=RequestMethod.GET)
	public ModelAndView showByEmail(
			@PathVariable("email") String email
			){
		ModelAndView mav = new ModelAndView("/member/show");
		Member member = memberService.getMember(email);
		mav.addObject("member",member);
		
		return mav;
	}
	
	@RequestMapping(value="/member/result/{email}",method=RequestMethod.GET)
	public @ResponseBody  Map<String,String> checkEmail(
			@PathVariable("email") String email
			){
		
		logger.debug("email :  {}.", email);
		Map<String,String> resultMap = new HashMap();
		Member member = memberService.getMember(email);
		if(member ==null){
			logger.debug("erroraaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa {}.", "hahahah");
		}
		
		return resultMap;
	}
	
}
