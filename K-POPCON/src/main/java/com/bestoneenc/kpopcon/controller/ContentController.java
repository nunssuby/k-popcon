package com.bestoneenc.kpopcon.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.bestoneenc.kpopcon.service.ContentService;
import com.bestoneenc.kpopcon.vo.Category;
import com.bestoneenc.kpopcon.vo.Content;
import com.bestoneenc.kpopcon.vo.ContentBox;
import com.bestoneenc.kpopcon.vo.AttachedFile;
import com.bestoneenc.kpopcon.vo.Scope;
import com.bestoneenc.kpopcon.vo.Tag;
import com.bestoneenc.kpopcon.vo.Member;

@Controller
@RequestMapping("/content/**")
public class ContentController {

	private static final Logger logger = LoggerFactory.getLogger(ContentController.class);
	
	@Autowired
	ContentService contentService;
	@Inject
	private FileSystemResource fsResource;

	@RequestMapping(value = "/content/form", method = RequestMethod.GET)
	public String form(HttpServletRequest request, Model model) {
		
		HttpSession session = request.getSession();
		Member member = (Member) session.getAttribute("member");

		List<Category> categories =  contentService.getCategories();
		List<ContentBox> contentBoxes = contentService.getContentBoxes(member);
		List<Scope> contentScopes = contentService.getContentScopes();
			
		model.addAttribute("categories",categories);
		model.addAttribute("contentBoxes",contentBoxes);
		model.addAttribute("contentScopes",contentScopes);
		
		return "content/create";
	}
	
	@RequestMapping(value="/content", method = RequestMethod.POST)
	public @ResponseBody int create(@ModelAttribute("content") Content content , BindingResult result, HttpSession session) {
		Member member = (Member) session.getAttribute("member");
		
		logger.debug("title : {}", content.getTitle());
		logger.debug("file size : {}", content.getFiles().size());
//		logger.debug("file size : {}", content.getContentFiles().size());
		logger.debug("file path : {}", fsResource.getPath());
		logger.debug("getCategory : {}", content.getCategory());
		logger.debug("getCategoryId : {}", content.getCategoryId());
		logger.debug("memberId : {}", member.getMemberId());
		
		
		// TODO thumbnail, contentBox설정, Member조회 기능 필요
//		ContentBox contentBox = contentService.getContentBox(contentBoxId);
//		Member member = new Member();
//		member.setMemberId(1);
//		String[] tags = content.getTagWithComma().split(",");
//		content.setTags(content.stringArrayToListConvertForTag(tags));
		content.tagWithCommaConvertToTagsInList();
		content.setMember(member);
		
		for(MultipartFile multipartFile : content.getFiles()){
			AttachedFile contentFile = new AttachedFile(multipartFile);
			content.getContentFiles().add(contentFile);
			String directory = fsResource.getPath()+member.getMemberId()+File.separator;
			logger.debug("path : {}", directory);
			File dest= new File(directory);
			if (!dest.exists()) { //존재하지 않으면
				dest.mkdirs(); //생성
			}
			try {
				multipartFile.transferTo(new File(directory + contentFile.getUuidFullName()));
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		contentService.createContent(content);
		return content.getContentId();
	}
	
	@RequestMapping(value="/content", method=RequestMethod.GET)
	public ModelAndView list(){
		ModelAndView mav = new ModelAndView("/content/list");
		List<Content> contents = contentService.getContents();
		mav.addObject("contents",contents);
		
		return mav;
	}

	/*@RequestMapping(value = "/content", method = RequestMethod.POST)
	public @ResponseBody Content create(
			HttpServletRequest request,
			HttpServletResponse response
			, @RequestParam("title") String title
			, @RequestParam("youtubeLink") String youtubeLink
			, @RequestParam("category") int category
			, @RequestParam("description") String description
			, @RequestParam("contentBoxId") int contentBoxId
			, @RequestParam("tag") String tag
			,@RequestParam("scope") int scope
			, @RequestParam("isSns") boolean isSns
			,@RequestParam("file") MultipartFile file) throws IOException {

		
		System.out.println("okok");
		
		
		// TODO thumbnail도 필요함
		ContentBox contentBox = contentService.getContentBox(contentBoxId);
		Member member = new Member();
		member.setMemberId(1);
		
		Content content = new Content();
		content.setTitle(title);
		content.setYoutubeLink(youtubeLink);
		content.setCategory(Category.valueOf(category));
		content.setDescription(description);
		content.setScope(Scope.valueOf(scope));
		content.setContentBox(contentBox);
		String[] tags = tag.split(",");
		content.setTags(content.stringArrayToListConventorForTag(tags));
		content.setMember(member);
		
		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();

				// Creating the directory to store file
				String rootPath = System.getProperty("catalina.home");
				File dir = new File(rootPath + File.separator + "tmpFiles");
				if (!dir.exists())
					dir.mkdirs();

				// 실제 파일명
				String originFileName = file.getOriginalFilename();

				// 확장자
				int pos = originFileName.lastIndexOf(".");
				String extension = originFileName.substring(pos + 1);
				// 저장할 파일명
				String savedFileName = getUniqueFileName(extension, 1);

				// Create the file on server
				File serverFile = new File(dir.getAbsolutePath() + File.separator + savedFileName);
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

				logger.info("Server File Location=" + serverFile.getAbsolutePath());

				// return "You successfully uploaded file=" + name;
			} catch (Exception e) {
				// return "You failed to upload " + name + " => " +
				// e.getMessage();
			}
		} else {
			// return "You failed to upload " + name+
			// " because the file was empty.";
		}
		String imageURL="url";	// TODO : 나중에 파일 다운로드 url을 어떻게 할지 결정
		content.setImageUrl(imageURL);
		
		contentService.createContent(content);
		

		return content;

	}*/
	
//	private String getExtension

	private String getUniqueFileName(String extension, int count) {
		long randomSeed = 100000000L;
		Random random = new Random();

		randomSeed = Math.abs(random.nextLong());
		String result = ((new java.util.Date()).getTime() + randomSeed) + String.valueOf(count) + "." + extension;

		return result;
	}
	
	
}
