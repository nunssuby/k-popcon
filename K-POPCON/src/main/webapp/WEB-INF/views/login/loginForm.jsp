<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="true"%>
<!DOCTYPE html>
<html>
<head>
<title>BESTONE E&C</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
<link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet" media="screen">

</head>
<body>
	<div class="page-container">
		<div class="input_box">
			<form id="loginForm" name="loginForm"  action="javascript:isLogin();">
				<h1>K-POPCON</h1>
				<input type="text" name="email" class="email" required placeholder="Username"> 
				<input type="password" name="password" class="password" required placeholder="Password">
				<div class="save_id">
					<input type="checkbox" id="save_id"> <label for="save_id">Remember Me</label>
				</div>
				<button id="submit" class="button"><span>Log In</span></button>
			</form>
		</div>
	</div>

	<!-- jQuery (부트스트랩의 자바스크립트 플러그인을 위해 필요한) -->
	<script src="<c:url value="/resources/js/jquery-1.11.2.min.js"/>"></script>
	<!-- 모든 합쳐진 플러그인을 포함하거나 (아래) 필요한 각각의 파일들을 포함하세요 -->
	<script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
	<script src="<c:url value="/resources/js/jquery.validate.min.js"/>"></script>

	<!-- Respond.js 으로 IE8 에서 반응형 기능을 활성화하세요 (https://github.com/scottjehl/Respond) -->
	<!--     <script src="resources/js/respond.js"></script> -->
	<script>
		//onsubmit="validation()"
		$(function() {
			$("loginForm").validate();
		})
		// 		function validation(){
		// 			if ($("#password").val())

		// 		}

		function isLogin() {
			var data = $("#loginForm").serialize();
			$.post("loginResult", data ,function(data, status) {
// 				alert("Data: " + data + "\nStatus: " + status);
				if(status=='success'){
					console.log(data);
// 					console.log(data.result);
// 					data = $.parseJSON(data);
// 					alert(data.result);
// 					$.each(data, function(key, value){
// 					    alert('key:' + key + ' / ' + 'value:' + value);
// 					});
					if(data.result)
						$(location).attr('href','<c:url value="/"/>');
					else ;
						// TODO 로그인 false 구현
				} else ;
					// TODO 로그인 실패 구현
			});
		}

	</script>
</body>
</html>