<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:directive.include file="/WEB-INF/views/includes.jsp"/>
<jsp:directive.include file="/WEB-INF/views/header.jsp"/>

</head>
<body>
	회원관리
    <div>
	    <span>seq</span>
		<span>id</span>
		<span>이름</span>
		<span>성별</span>
		<span>생년월일</span>
		<span>국적</span>
		<span>구분</span>
		<span>상태</span>
		<span>암호</span>
		<span>정지</span>
    </div>
    <c:forEach var="member" items="${members}" varStatus="status">
    	<form:form id="member_${member.memberId}" name="member_${member.memberId}" method="PUT" action="/kpopcon/admin/member" modelAttribute="member" >
			<div>
				<span>
					${status.count}
				</span>
				<span>
					${member.email}
				</span>
				<span>
					${member.name}
				</span>
				<span>
					${member.birthday}
				</span>
				<span>
					${member.nationalityId}
				</span>
				<span>
					${member.loginType}
				</span>
				<span>
					<c:if test ="${member.loginType == 'KP'}">
						<input type="button" value="변경" onclick="showBannerForm()"> 
					</c:if>
				</span>
				<span>
					<c:choose>
						<c:when test="${member.status == 'Y' }">
							<input type="button" value="정지" onclick="updateStatus(${member.memberId})"> 
						</c:when>
						<c:otherwise>
							<input type="button" value="해제" onclick="updateStatus(${member.memberId})"> 
						</c:otherwise>
					</c:choose>
					
				</span>
			</div>
		</form:form>
	</c:forEach>
	
	<script>
	function showBannerForm() {
		$(location).attr('href','<c:url value="/admin/banner/form"/>');
	}
	function updateStatus(bannerId) {
		var url = '<c:url value="/admin/banner/"/>';
// 		url += '?bannerId=';
		url += bannerId;
		$(location).attr('href',url);
	}
	function updateStatus(memberId){
// 		alert("1");
// 		$(location).attr('href','<c:url value="/admin/member/"/>'+memberId);
// 		var fromName =
/*  		var data = $("#member_"+memberId).serialize();
		$.post("loginResult", data ,function(data, status) {
//				alert("Data: " + data + "\nStatus: " + status);
			if(status=='success'){
				console.log(data);
//					console.log(data.result);
//					data = $.parseJSON(data);
//					alert(data.result);
//					$.each(data, function(key, value){
//					    alert('key:' + key + ' / ' + 'value:' + value);
//					});
				if(data.result)
					$(location).attr('href','<c:url value="/"/>');
				else ;
					// TODO 로그인 false 구현
			} else ;
				// TODO 로그인 실패 구현
		}); */ 
		var url = '<c:url value="/admin/member/"/>'+ memberId + '/status';
  		$.ajax({
		    url: url,
		    type: 'PUT',
		    success: function(data, status) {
		    	alert("status : "+status);
		    	if(status=='success'){
					console.log(data);
						// TODO 로그인 false 구현
				}
		    }
		});  
// 		$("#member_"+memberId).submit();
		
	}
	</script>
	<script src="<c:url value="/resources/js/jquery-1.11.2.min.js"/>"></script>
	<script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
	<script src="<c:url value="/resources/js/jquery.validate.min.js"/>"></script>

<jsp:directive.include file="/WEB-INF/views/footer.jsp"/>