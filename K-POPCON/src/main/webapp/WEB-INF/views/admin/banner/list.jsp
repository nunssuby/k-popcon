<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:directive.include file="/WEB-INF/views/includes.jsp"/>
<jsp:directive.include file="/WEB-INF/views/header.jsp"/>

</head>
<body>
	Register Contents
    <div>
	    <span>순서</span>
		<span>제목</span>
		<span>게시기간</span>
		<span>언어</span>
		<span>게시여부</span>
    </div>
    <c:forEach var="banner" items="${banners}" varStatus="status">
		<div>
<%-- 			<input type="hidden" id="${status.index}" value="${banner.bannerId}"> --%>
			<span>
				${banner.sequence}
			</span>
			<span onclick="showBannerView(${banner.bannerId});">
				${banner.title}
			</span>
			<span>
				${banner.startDate} ~ ${banner.endDate}
			</span>
			<span>
				${banner.language}
			</span>
			<span>
				${banner.isValid}
			</span>
		</div>
	</c:forEach>
	<input type="button" value="등록" onclick="showBannerForm()"> 
	
	<script>
	function showBannerForm() {
		$(location).attr('href','<c:url value="/admin/banner/form"/>');
	}
	function showBannerView(bannerId) {
		var url = '<c:url value="/admin/banner/"/>';
// 		url += '?bannerId=';
		url += bannerId;
		$(location).attr('href',url);
	}
	</script>
	<script src="<c:url value="/resources/js/jquery-1.11.2.min.js"/>"></script>
	<script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
	<script src="<c:url value="/resources/js/jquery.validate.min.js"/>"></script>

<jsp:directive.include file="/WEB-INF/views/footer.jsp"/>