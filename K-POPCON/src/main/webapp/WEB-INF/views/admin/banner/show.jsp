<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:directive.include file="/WEB-INF/views/includes.jsp"/>
<jsp:directive.include file="/WEB-INF/views/header.jsp"/>

</head>
<body>
	Register Contents
	<div>
		<span>순서</span><span>${banner.sequence} </span>
		<span>제목</span><span>${banner.title} </span>
	</div>
	<div>
		<span>게시기간</span><span>${banner.startDate}~${banner.endDate} </span>
		<span>게시언어</span><span>${banner.language} </span>
	</div>
	<div>
		<span>게시여부</span><span>${banner.isValid} </span>
		<span>배너파일</span><span>${banner.link} </span>
	</div>
	<div>
		${banner.link} 
	</div>
	<div>
		${banner.image} 
	</div>
        
        
    <input type="button" value="목록" onclick="showBannerList()"> 
    <input type="button" value="수정" onclick="showBannerEdit()"> 
    <input type="button" value="삭제" onclick="showBannerDelete(${banner.bannerId})"> 
	
	<script>
	function showBannerList() {
		var url = '<c:url value="/admin/banner/main/list"/>';
// 		url += '?bannerType=';
// 		url += '${banner.bannerType}' ;
		$(location).attr('href',url);
		
	}
	function showBannerEdit() {
		$(location).attr('href','<c:url value="/admin/banner/form"/>');
	}
	function showBannerDelete(id) {
		var url = '<c:url value="/admin/banner/"/>'+id; 
		
		$.ajax({
		    url: url,
		    type: 'DELETE',
		    success: function(data, status) {
		    	if(status=='success'){
					console.log(data);
//						console.log(data.result);
//						data = $.parseJSON(data);
//						alert(data.result);
//						$.each(data, function(key, value){
//						    alert('key:' + key + ' / ' + 'value:' + value);
//						});
					if(data.result>0)
						alert("삭제되었음");
					else ;
						// TODO 로그인 false 구현
				}
		    }
		});
		
/* 		$.delete("<c:url value="/banner/BannerForm"/>" ,function(data, status) {
//				alert("Data: " + data + "\nStatus: " + status);
			if(status=='success'){
				console.log(data);
//					console.log(data.result);
//					data = $.parseJSON(data);
//					alert(data.result);
//					$.each(data, function(key, value){
//					    alert('key:' + key + ' / ' + 'value:' + value);
//					});
				if(data.result>0)
					alert("삭제되었음");
				else ;
					// TODO 로그인 false 구현
			} else ;
				// TODO 로그인 실패 구현
		}); */
	}
	</script>
	<script src="<c:url value="/resources/js/jquery-1.11.2.min.js"/>"></script>
	<script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
	<script src="<c:url value="/resources/js/jquery.validate.min.js"/>"></script>
<jsp:directive.include file="/WEB-INF/views/footer.jsp"/>