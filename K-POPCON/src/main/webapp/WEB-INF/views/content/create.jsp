<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
<title>BESTONE E&C</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
<link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet" media="screen">
</head>
<body>
	Register Contents
	
<%-- 	<form:form id="contentForm" name="contentForm" method="POST" action="/kpopcon/content" enctype="multipart/form-data" modelAttribute="content"> --%>
<%--     <form:form id="contentForm" name="contentForm" method="POST" action="javascript:create();" enctype="multipart/form-data" modelAttribute="content"> --%>
        <form:form id="contentForm" name="contentForm" method="POST" enctype="multipart/form-data" modelAttribute="content">
        title: <input type="text" name="title"><br />
        Image: <input type="file" name="files" multiple><br /> 
<!--         Image: <input type="file" name="files[1]"><br />  -->
        Youtube Link: <input type="text" name="youtubeLink"><br />
        Category: 
		<select name="categoryId" class="form-control fw_bold">
			<option value="default">Category 선택</option>
			<c:forEach var="category" items="${categories}" varStatus="status">
				<option value="${category.value}">${category}</option>
			</c:forEach>
		</select>
<br />
        Description: <input type="text" name="description"><br />
        Contents Box: 
		<select name="contentBoxId" class="form-control fw_bold">
			<option value="default">Contents Box 선택</option>
			<c:forEach var="contentBox" items="${contentBoxes}" varStatus="status">
				<option value="${contentBox.boxId}">${contentBox.name}</option>
			</c:forEach>
		</select>
<br />
        Tag: <input type="text" name="tagWithComma"><br />
        Public Setting: 
        	<select name="scopeId" class="form-control fw_bold">
				<option value="default">Public Setting 선택</option>
				<c:forEach var="contentScope" items="${contentScopes}" varStatus="status">
					<option value="${contentScope.value}">${contentScope}</option>
				</c:forEach>
			</select>
        <br />
        <input type="checkBox" name="isSns"> SNS
        
        
        <input type="submit" value="Register"> 
        
    </form:form>
   
	<script src="<c:url value="/resources/js/jquery-1.11.2.min.js"/>"></script>
	<script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
	<script src="<c:url value="/resources/js/jquery.validate.min.js"/>"></script>
	<script>
/*       function create(form){
// 		var data = $("#contentForm").serialize();
		var form = $("#contentForm");
		var data = new FormData(form);
		console.log(data);
		$.post("<c:url value='/content'/>", data ,function(content, status) {
	//			alert("Data: " + data + "\nStatus: " + status);
			if(status=='success'){
				if($.isNumeric(content.contentId)&&content.contentId>0)
					alert("content를 더 올리겠습니까");
// 				else ;
					// TODO 로그인 false 구현
			} else ;
				// TODO 로그인 실패 구현
		});
    }   */ 
    
    
    $('#contentForm')
      .submit( function( e ) {
    	  $.ajax({
      	    url: "<c:url value='/content'/>",
      	    type: 'POST',
      	    	data: new FormData( this ),
      	    contentType: false,
      	    processData: false,
//       	    dataType: 'json',
      	    success: callback
      	});
        e.preventDefault();
    } );  
    
    function callback(contentId, status){
    	if(status=='success'){
    		if($.isNumeric(contentId)&&contentId>0)
    			alert("content를 더 올리겠습니까");
    	}
    }
      
      
      
     
/*      $('#contentForm')
     .submit( function( e ) {
    	 create(this);
       e.preventDefault();
     } );  */
    
/*      $('#contentForm')
    .submit( function( e ) {
      $.ajax( {
        url: "<c:url value='/content'/>",
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false
      } );
      e.preventDefault();
    } );  */
    
    
    </script>
</body>
</html>