<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<title>BESTONE E&C</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
<link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet" media="screen">


</head>
<body>
	Join
	
	<form:form id="joinForm" name="joinForm" method="POST" modelAttribute="member" >
		<div>
			<span>Email(ID)</span>
			<span><input type="text" name="email" placeholder="Enter your email address to be used as ID"></span>
		</div>
		<div>
			<span>Password</span>
			<span><input type="text" name="password" ></span>
		</div>
		<div>
			<span>Verify Password</span>
			<span><input type="text" name="verify_password"></span>
		</div>
		<div>
<!-- 			<input type="submit" value="Register" />  -->
			<input type="submit" value="Join" >
		</div>
	</form:form>
	<div>
		<span><input type="button" value="twitter" onclick="twitterLogin()"> </span> 
		<span><input type="button" value="facebook" onclick="facebookLogin()"></span>
		<span><input type="button" value="google" onclick="googleLogin()"> </span> 
		<span><input type="button" value="instagram" onclick="instagramLogin()"></span>
	</div>
	<div>
		<span>Kpopcon</span>
	</div>
	<div>
		<span>Join조인</span>
	</div>
	<div>
		<span>Find the Password</span>
	</div>

	<script src="<c:url value="/resources/js/jquery-1.11.2.min.js"/>"></script>
	<script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
	<script src="<c:url value="/resources/js/jquery.validate.min.js"/>"></script>
	<script src="<c:url value="/resources/js/oauth.js"/>"></script>
	<script>
			$('#joinForm')
		    .submit( function( e ) {
		  	  $.ajax({
		    	    url: '<c:url value="/member"/>',
		    	    type: 'POST',
		    	    	data: new FormData( this ),
		    	    contentType: false,
		    	    processData: false,
		//     	    dataType: 'json',
		    	    success: callback
		    	});
		      e.preventDefault();
		  } );
			
		function callback(data, status){
	    	if(status=='success'){
	    		
	    		if(data.isMember){
	    			alert("이미존재하는 email입니다. 다른 email을 넣으세요");
	    		}
	    		if($.isNumeric(data.memberId)&&data.memberId>0){
	    			var url = '<c:url value="/member/"/>';
	    			url+= data.memberId;
	    			url+="/information/form";
	    			$(location).attr('href',url);
	    		}
	    	}
	    }
	
		OAuth.initialize('6H4QhBovJajmvuxej8-SeoUBq60');

		function twitterLogin() {
			OAuth.redirect('twitter',
					"http://localhost:8080/kpopcon/auth/twitter");
		}

		OAuth.callback('twitter').done(function(result) {
			//use result.access_token in your API request
			//or use result.get|post|put|del|patch|me methods (see below)
			console.log("twitter result : "+result);
		}).fail(function(err) {
			//handle error with err
		});

		function facebookLogin() {
			OAuth.redirect('facebook',
					"http://localhost:8080/kpopcon/auth/facebook");
		}

		OAuth.callback('facebook').done(function(result) {
			//use result.access_token in your API request
			//or use result.get|post|put|del|patch|me methods (see below)
			console.log("facebook result : "+result);
		}).fail(function(err) {
			//handle error with err
		});

		function googleLogin() {
			OAuth.redirect('google',
					"http://localhost:8080/kpopcon/auth/google");
		}

		OAuth.callback('google').done(function(result) {
			//use result.access_token in your API request
			//or use result.get|post|put|del|patch|me methods (see below)
			console.log("google result : "+result);
		}).fail(function(err) {
			//handle error with err
		});

		function instagramLogin() {
			OAuth.redirect('instagram',
					"http://localhost:8080/kpopcon/auth/instagram");
		}

		OAuth.callback('instagram').done(function(result) {
			//use result.access_token in your API request
			//or use result.get|post|put|del|patch|me methods (see below)
			console.log("instagram result : "+result);
		}).fail(function(err) {
			//handle error with err
		});
	</script>
	<jsp:directive.include file="/WEB-INF/views/footer.jsp" />
