<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<title>BESTONE E&C</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
<link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet" media="screen">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

</head>
<body>
	Join
	
	<c:url var="post_url"  value="/member/information" />
	<form:form id="informationForm" action="${post_url}" name="informationForm" modelAttribute="member" method="put">
		<input type="hidden" name="memberId" value="${member.memberId}">
		<div>
			<span>Name</span>
			<span><input type="text" name="name"/></span>
		</div>
		<div>
			<span>Gender</span>
			<span><input type="text" name="gender" ></span>
		</div>
		<div>
			<span>Birthday</span>
			<span> <input type="text" id="datepicker" name="birthday"></span>
		</div>
		<div>
			<span>Nationality</span>
			<span><input type="text" name="nationalityId"></span>
		</div>
		<div>
<!-- 			<input type="submit" value="Register" />  -->
			<input type="submit" value="Complete" >
		</div>
	</form:form>

	<script src="<c:url value="/resources/js/jquery-1.11.2.min.js"/>"></script>
	<script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
	<script src="<c:url value="/resources/js/jquery.validate.min.js"/>"></script>
	<script src="<c:url value="/resources/js/oauth.js"/>"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script>
		$(function() {
		    $( "#datepicker" ).datepicker({
		      showOn: "button",
		      buttonImage: '<c:url value="/resources/images/calendar.png"/>',
		      buttonImageOnly: true,
		      buttonText: "Select date",
		      dateFormat: "yy-mm-dd"
		    });
		});
		
	/* 	$(function() {
		    $( "#datepicker" ).datepicker({
		      showOn: "button",
		      buttonImage: '<c:url value="/resources/images/calendar.png"/>',
		      buttonImageOnly: true,
		      buttonText: "Select date",
		      format: "yy-mm-dd"
		    });
		}); */
		
		
//  		 $('#informationForm').submit( function( e ) {
// 		  	  $.ajax({
// 		    	    url: '<c:url value="/member/information"/>',
// 		    	    type: 'PUT',
// 		    	    	data: new FormData( this ),
// 		    	    contentType: false,
// 		    	    processData: false,
// 		//     	    dataType: 'json',
// 		    	    success: callback
// 		    	});
// 		      e.preventDefault();
// 		  } );
			
// 		function callback(isUpdate, status){
// 	    	if(status=='success'){
	    		
// 	    		if(isUpdate){
// 	    			alert("성공");
// 	    		}else {
// 	    			alert("업데이트가 제대로 되지 않았음");
// 	    		}
// 	    	}
// 	    }  
	
	</script>
	<jsp:directive.include file="/WEB-INF/views/footer.jsp" />
