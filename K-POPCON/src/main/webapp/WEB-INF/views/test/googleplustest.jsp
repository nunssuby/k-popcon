<!DOCTYPE html>
<html>
<head>
<title>Facebook Login JavaScript Example</title>
<meta charset="UTF-8">
</head>
<body>

<!-- 로그인 버튼 -->
<span id="signinButton">
  <span
    class="g-signin"
    data-callback="signinCallback"
    data-clientid="21230173464-ogojialr61o10116dmhvoavea8klb1d6.apps.googleusercontent.com"
    data-cookiepolicy="none"
    data-requestvisibleactions="http://schemas.google.com/AddActivity"
    data-approvalprompt="force"
    data-scope="https://www.googleapis.com/auth/plus.login">
  </span>
</span>

<!-- 버튼을 렌더링할 위치에 태그 배치 -->
<!-- <button -->
<!--   class="g-interactivepost" -->
<!--   data-contenturl="https://plus.google.com/pages/" -->
<!--   data-contentdeeplinkid="/pages" -->
<!--   data-clientid="21230173464-ogojialr61o10116dmhvoavea8klb1d6.apps.googleusercontent.com" -->
<!--   data-cookiepolicy="single_host_origin" -->
<!--   data-prefilltext="Engage your users today, create a Google+ page for your business." -->
<!--   data-calltoactionlabel="CREATE" -->
<!--   data-calltoactionurl="http://plus.google.com/pages/create" -->
<!--   data-calltoactiondeeplinkid="/pages/create"> -->
<!--   Tell your friends -->
<!-- </button> -->

<script>
//google+ 사용 설정
(function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/client:plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
  
//로그인 콜백  
function signinCallback(authResult) {
	  if (authResult['access_token']) {
	    // 승인 성공
	    // 사용자가 승인되었으므로 로그인 버튼 숨김. 예:
	    document.getElementById('signinButton').setAttribute('style', 'display: none');
// 	    $(location).attr('href','<c:url value="/"/>');
	  } else if (authResult['error']) {
	    // 오류가 발생했습니다.
	    // 가능한 오류 코드:
	    //   "access_denied" - 사용자가 앱에 대한 액세스 거부
	    //   "immediate_failed" - 사용자가 자동으로 로그인할 수 없음
	    // console.log('오류 발생: ' + authResult['error']);
	  }
	}
</script>


<script src="<c:url value="/resources/js/jquery-1.11.2.min.js"/>"></script>
	<script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
	<script src="<c:url value="/resources/js/jquery.validate.min.js"/>"></script>
</body>
</html>