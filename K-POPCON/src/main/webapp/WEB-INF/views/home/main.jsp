<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="true"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- <link rel="icon" href="../../favicon.ico"> -->
    <title>Off Canvas Template for Bootstrap</title>
    <!-- Bootstrap core CSS -->
    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<c:url value="/resources/css/offcanvas.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/general.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/custom.css"/>" rel="stylesheet">
  </head>
  <body>
    <nav class="navbar navbar-fixed-top navbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Project name</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>
          </ul>
        </div><!-- /.nav-collapse -->
      </div><!-- /.container -->
    </nav><!-- /.navbar -->
    <div class="container">

      <div class="row row-offcanvas row-offcanvas-right">
		<div class="col-xs-12 banner_kp">
          	<p class="pull-right visible-xs">
				<button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
			</p>
          	<div id="carousel-example-generic" class="carousel slide col-sm-74-7-kp pr0 pl0" data-ride="carousel">
				<!-- Indicators -->
				<ol class="carousel-indicators">
					<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
					<li data-target="#carousel-example-generic" data-slide-to="1"></li>
					<li data-target="#carousel-example-generic" data-slide-to="2"></li>
				</ol>
				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">
					<div class="item active">
						<img src="..." alt="...">
				    	<div class="carousel-caption">
						</div>
					</div>
					<div class="item">
						<img src="..." alt="...">
						<div class="carousel-caption">
						  ...
						</div>
					</div>
				</div>
				<!-- Controls -->
				<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev"> 
					<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> 
					<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
          	<div class="col-sm-24-3-kp sidebar-offcanvas pull-right pr0 pl0" id="sidebar">
				<div class="list-group">
					<a href="#" class="list-group-item active">Link</a>
					<a href="#" class="list-group-item">Link</a>
					<a href="#" class="list-group-item">Link</a>
					<a href="#" class="list-group-item">Link</a>
					<a href="#" class="list-group-item">Link</a>
				</div>
			</div><!--/.sidebar-offcanvas-->
		</div>
        <div class="col-xs-12 content_box_kp">
			<c:forEach var="content" items="${contents}" varStatus="status">
				<div class="content_kp">
					<h2>${content.title}</h2>
					<p>
						<img src="<c:url value="/image/${content.contentFiles[0].fileId}"/>" />
					</p>
					<!--               		<p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p> -->
				</div>
			</c:forEach>
        </div><!--/.col-xs-12.col-sm-9-->
      </div><!--/row-->
      <hr>
      <footer>
        <p>&copy; Company 2014</p>
      </footer>
    </div><!--/.container-->

    <script src="resources/js/jquery-1.11.2.min.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>
    <script src="resources/js/offcanvas.js"></script>
    <script>
    	$(document).ready(function () {
    		// .content_kp 배치
    		$.each($('.content_kp'), function(index) {
    			var content= $(this);
    			determinAbsoluteLeftByContents(content,index);
    			// top 처리
    			var prev_top = $('.content_kp').eq(index-4).css('top');
    			var prev_height = $('.content_kp').eq(index-4).height(); 
    			
    			$(this).css('top', prev_top+prev_height);
    		});
    	});
    	
    	function determinAbsoluteLeftByContents(content,index){
			var col = index%4;
			if(col == 0) {
				content.css('left', 0);
			} else if(col == 1) {
				content.css('left', 250);
			} else if(col == 2) {
				content.css('left', 500);
			} else {
				content.css('left', 750);
			}
    	}
    </script>
  </body>
</html>